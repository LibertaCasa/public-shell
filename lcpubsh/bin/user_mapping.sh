#!/bin/bash
# Original by https://github.com/sleeepyjack/dockersh
# Modified by georg@lysergic.dev

if [ -z "${HOST_USER_NAME}" -o -z "${HOST_USER_ID}" -o -z "${HOST_USER_GID}" ]; then
        echo "HOST_USER_NAME, HOST_USER_ID & HOST_USER_GID needs to be set!"; exit 100
fi
useradd \
      --uid ${HOST_USER_ID} \
      -U \
      -m \
      -s /bin/bash \
      ${HOST_USER_NAME}
sleep 3s

echo ${HOST_USER_NAME}:${HOST_USER_NAME} | chpasswd

exec su - "${HOST_USER_NAME}"
